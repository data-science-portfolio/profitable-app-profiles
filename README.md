# Profitable App Profiles

Our goal for this project is to analyze data to help our developers understand what type of apps are likely to attract more users.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-portfolio%2Fprofitable-app-profiles/main)
